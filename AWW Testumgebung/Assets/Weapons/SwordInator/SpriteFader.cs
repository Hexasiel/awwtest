﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteFader : MonoBehaviour
{
    SpriteRenderer sr;
    float spriteOpacity = 1f;
    public float spriteLifeTime = 2f;

    private void Start() {
        sr = transform.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        spriteOpacity -= 1 / spriteLifeTime * Time.deltaTime;
        sr.color = new Color(1f, 1f, 1f, spriteOpacity);
    }
}
