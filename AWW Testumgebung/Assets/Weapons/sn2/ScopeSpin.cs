﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScopeSpin : MonoBehaviour
{
    public bool invertRotation = false;
    public float rotationSpeed = 10f;

    // Update is called once per frame
    void Update()
    {
        if (invertRotation) {
            transform.Rotate(Vector3.forward, rotationSpeed * Time.deltaTime);
        }
        else {
            transform.Rotate(Vector3.forward, -1 * rotationSpeed * Time.deltaTime);
        }
       
    }
}
