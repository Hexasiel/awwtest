﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //External references
    public CharacterController charController;
    public Transform groundCheck;

    //Settings
    public float movementSpeed = 50f;
    public float sprintMultiplier = 1.5f;
    float gravityMultiplier = -9.81f * 10f;
    public float groundDistance = 0.4f;
    float jumpHeight = 4f;

    //Functionality
    bool isGrounded;
    Vector3 movementDirection;
    Vector3 lastPosition = new Vector3(0, 0, 0);

    //Readable Functionality
    public bool isMoving = false;
    public Vector3 velocity;


    void Start()
    {
        charController = GetComponent<CharacterController>();
    }


    void Update()
    {
        //Checking if player is standing on the ground
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance);
        if (isGrounded && velocity.y < 0) {
            velocity.y = -2f;
        }

        //Getting input data
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        //Calculating movement direction
        movementDirection = transform.right * x + transform.forward * z;

        //Checkign for input and applying horizontal movement
        if (Input.GetButtonDown("Jump") && isGrounded) {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravityMultiplier);
        }
        if (Input.GetButton("Sprint")) { 
            charController.Move(movementDirection * movementSpeed * sprintMultiplier * Time.deltaTime);
        }
        else {
            charController.Move(movementDirection * movementSpeed * Time.deltaTime);
        }

        //Modifying and applying vertical velocity
        velocity.y += gravityMultiplier * Time.deltaTime ;
        charController.Move(velocity * Time.deltaTime);

        //Checking is the player is currently moving
        if(lastPosition.x != transform.position.x && lastPosition.z != transform.position.z) {
            isMoving = true;
        }
        else {
            isMoving = false;
        }
        lastPosition = this.transform.position;
    }
}
