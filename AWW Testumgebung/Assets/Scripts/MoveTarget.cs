﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTarget : MonoBehaviour
{
    public GameObject EndPunkt;
    public float loopTime = 3.0f;
    Vector3 pointB;

    IEnumerator Start()
    {
        pointB = EndPunkt.transform.position;
        Vector3 pointA = transform.position;
        while (true)
        {
            yield return StartCoroutine(MoveObject(transform, pointA, pointB, loopTime));
            yield return StartCoroutine(MoveObject(transform, pointB, pointA, loopTime));
        }
    }

    IEnumerator MoveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
    {
        float f = 0.0f;
        float rate = 1.0f / time;
        while (f < 1.0f)
        {
            f += Time.deltaTime * rate;
            thisTransform.position = Vector3.Lerp(startPos, endPos, f);
            yield return null;
        }
    }

}
