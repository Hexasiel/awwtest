﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour
{
    public Vector3 targetPoint;
    public float speed;
    public GameObject hitEffect;
    public LayerMask hitLayer;
    public float damage;

    Vector3 lastPos;

    private void Start() {
        lastPos = transform.position;
    }

    private void Update() {

        if(transform.position == targetPoint) {
            HitNothing();
        }
        transform.position = Vector3.MoveTowards(transform.position, targetPoint, speed * Time.deltaTime);

        RaycastHit hit;
        Vector3 direction = transform.position - lastPos;
        if(Physics.Raycast(transform.position, direction, out hit, direction.magnitude, hitLayer)) {
            HitTarget(hit.transform, hit.point);
        }

        lastPos = transform.position;
    }

    public void SetupBullet(Vector3 target, GameObject effect) {
        targetPoint = target;
        if(hitEffect == null) {
            hitEffect = effect;
        }
    }

    void HitNothing() {
        Destroy(gameObject);
    }
    void HitTarget(Transform hitObject, Vector3 hitPosition) {
        Debug.Log("Test");

        if (hitObject.transform.parent.parent.CompareTag("Feedback")) {
            hitObject.transform.parent.parent.GetComponent<HitFeedback>().TakeDamage(damage);
        }

        GameObject hit = Instantiate(hitEffect, hitPosition, transform.rotation);
        hit.transform.SetParent(hitObject);
        Destroy(gameObject);
        Destroy(hit, 2f);
    }

   


}
