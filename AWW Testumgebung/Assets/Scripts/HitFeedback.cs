﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitFeedback : MonoBehaviour
{
    //External References
    public Material materialNormal;
    public Material materialHit;
    public Material materialFrozen;
    public GameObject EndPunkt;

    //Settings
    public float dropTime = 1.0f;
    public float standTime = 5.0f;
    public float hitpoints = 100.0f;


    //Functionality
    Vector3 pointA, pointB;

    public float currentHitpoints;
    bool destroyed = false;
    bool ready = true;
    bool frozen = false;

    private void Start()
    {
        pointA = transform.position;
        pointB = EndPunkt.transform.position;
        currentHitpoints = hitpoints;
    }

    private void Update()
    {
        if (destroyed && ready)
        {
            ready = false;
            StartCoroutine(MoveObject(transform, pointA, pointB, dropTime));
        }
        else if (transform.position == pointB && !ready)
        {
            StartCoroutine(MoveObject(transform, pointB, pointA, standTime));           
        }
        else if (transform.position == pointA && !ready)
        {
            ready = true;
            destroyed = false;
            transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material = materialNormal;
            currentHitpoints = hitpoints;
        }
    }

    IEnumerator MoveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
    {
        float f = 0.0f;
        float rate = 1.0f / time;
        while (f < 1.0f)
        {
            f += Time.deltaTime * rate;
            thisTransform.position = Vector3.Lerp(startPos, endPos, f);
            yield return null;
        }
    }

    private void OnParticleCollision(GameObject other) {

            TakeDamage(1);

    }
   
    public void TakeDamage(float damage)
    {
        print("Damaged");
        if (ready)
        {
            if (frozen) {
                damage *= 100;
                frozen = false;
            }
            currentHitpoints -= damage;

            if (0.1f >= currentHitpoints)
            {
                destroyed = true;
                transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material = materialHit;
            }
            //else
            //{
            //    currentHitpoints -= damage;
            //}
            if (Weapon.activeWeapon.currentAW.aw_freezes) {
                frozen = true;
                transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material = materialFrozen;
            }
        }
       
    }

}
