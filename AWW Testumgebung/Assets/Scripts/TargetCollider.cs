﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetCollider : MonoBehaviour
{
    Weapon playerWeapon;
    HitFeedback parentsHitFeedback;
    List<ParticleCollisionEvent> particleCollisions;

    private void Start() {
        parentsHitFeedback = transform.parent.parent.GetComponent<HitFeedback>();
        particleCollisions = new List<ParticleCollisionEvent>();
        playerWeapon = Weapon.activeWeapon;
    }

    private void OnParticleCollision(GameObject other) {

        ParticlePhysicsExtensions.GetCollisionEvents(other.GetComponent<ParticleSystem>(), gameObject, particleCollisions);
        int collisionCount = particleCollisions.Count;
        Debug.Log("BAM" + collisionCount);
        for (int i = 0; i < collisionCount; i++) {
            parentsHitFeedback.TakeDamage(playerWeapon.currentAW.aw_Damage);
        }
    }
}
