﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Alien Weapon")]
public class AlienWeapon : ScriptableObject
{                                               //Default Values 
    [Header("Weapon Information")]
    public string aw_Name                       = "Weapon Name";
    public string aw_CreatorName1               = "Creator Name 1";
    public string aw_CreatorName2               = "Creator Name 2";
    public bool aw_isBeamWeapon                 = false;
    public bool aw_isFlamethrower               = false;
    public bool aw_IsSword                      = false;
    public bool aw_freezes                      = false;
    public int aw_FireModes                     = 1;
    public bool aw_UseHitScan                   = true;

    [Header("Transformable Weapons")]
    public bool aw_isTransformable              = false;
    public AlienWeapon aw_TransformedWeapon;
    public float aw_TransformTime               = 1f;

    [Header("Swords")]
    public float aw_ComboTime                   = 1f;

    [Header("Damage")]
    public float aw_Damage                      = 10;

    [Header("Transformation Adjustment")]
    public Vector3 aw_TransformAdjustment;
    public Vector3 aw_RotationAdjustment;

    [Header("Firerate and Bulletspread")]
    public float aw_Range                       = 100;
    public int aw_AmmoCapacity                  = 30;
    public int aw_AmmoUsedPerSecond             = 10;
    public float[] aw_FireRate                  = new float[3] {600,600,600};
    public int[] aw_MaxRoundsPerTrigger         = new int[3] { 0, 0, 0 };

    public float aw_ReloadTime                  = 2f;

    public float[] aw_SpreadFactor              = new float[3] { 0.005f, 0.005f, 0.005f };
    public float[] aw_MaximumSpread             = new float[3] { 0.05f, 0.05f, 0.05f };
    public float[] aw_ScopingSpreadMultiplier   = new float[3] { 0.5f, 0.5f, 0.5f };
    public float[] aw_RunningSpreadMultiplier   = new float[3] { 2, 2, 2 };
    public float[] aw_SpreadResetTime           = new float[3] { 1, 1, 1 };

    [Header("Beam Weapon")]
    public float aw_ChargeTime                  = 2f;

    [Header("Scope")]
    public float aw_ScopeZoomLevel              = 1.3f;
    public float aw_ScopeMouseSensitivity       = 1.0f;
    public float aw_ScopeMovespeedModifier      = 0.6f;

    public bool aw_HasScopeOverlay              = false;
    public bool aw_TurnOffCrosshair             = true;
    public Sprite aw_ScopeOverlayImage;

    [Header("Graphics (Old!!!)")]
    public Mesh aw_Mesh;
    public Material[] aw_Material               = new Material[1];
    [Header("Graphics")]
    public GameObject aw_Object;

    [Header("Bullets and Impact Effects (Graphic and Sound)")]
    public float aw_ImpactDelay                 = 0f;
    public GameObject aw_Bullet;
    public Vector3 aw_BulletInstantiationPoint;
    public float aw_BulletSpeed                 = 10f;
    public GameObject aw_ImpactEffect;
    public GameObject aw_ImpactEffect2;

    public AudioClip aw_ShootSound;
    public float aw_ShootSoundVolume            = 1f;
    public AudioClip aw_HitSound;
    public float aw_HitSoundVolume              = 1f;
    public AudioClip aw_ReloadSound;
    public float aw_ReloadSoundVolume = 1f;

    [Header("Animation")]
    public RuntimeAnimatorController aw_AnimationController;
    public Avatar aw_Avatar;
}
