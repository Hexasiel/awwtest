﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    //External references
    public Weapon weaponInHand;
    public GameObject pickupUI;

    //Settings
    public float distance = 10f;

    //Functionality
    GameObject detectedWeapon;
    bool isWeapon = false;

    void Update()
    {
        CheckWeapon();

        if(isWeapon)
        {
            if(Input.GetButtonDown("Pickup"))
            {
                weaponInHand.UpdateWeapon(detectedWeapon.GetComponent<WeaponInScene>().weaponInfo);
            }
        }
    }

    private void CheckWeapon()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward, out hit, distance))
        {
            if(hit.transform.tag == "Weapon")
            {
                pickupUI.SetActive(true);
                detectedWeapon = hit.transform.gameObject;
                isWeapon = true;
            }
        }
        else
        {
            pickupUI.SetActive(false);
            isWeapon = false;
        }
    }// Checks if player is currently looking at a weapon they can pick up
}
