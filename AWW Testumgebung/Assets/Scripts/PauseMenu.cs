﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    //External references
    public GameObject pauseMenuPanel;
    public GameObject creditsPanel;

    //Functionality
    public static bool GameIsPaused = false;
    public static bool creditsAreActive = false;
    Scene scene;

    private void Start() {
        scene = SceneManager.GetActiveScene();
    }

    void Update()
    {
        //Checking for Input
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (creditsAreActive) {
                HideCredits();
            }
            else if(GameIsPaused) {
                Resume();
            }
            else {
                Pause();
            }
        }
    }

    void Pause() {
        pauseMenuPanel.SetActive(true);
        GameIsPaused = !GameIsPaused;
        Time.timeScale = 0f;
        Cursor.lockState = CursorLockMode.None;
    }// Pauses the game and opens pause menu

    void Resume() {
        pauseMenuPanel.SetActive(false);
        GameIsPaused = !GameIsPaused;
        Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.Locked;
    }// Closes pause menu and resumes the game


    public void ResetDemo() {
        SceneManager.LoadScene(scene.name);
    }

    //Credits menu
    public void ShowCredits() {
        creditsAreActive = true;
        creditsPanel.SetActive(true);
    }
    public void HideCredits() {
        creditsAreActive = false;
        creditsPanel.SetActive(false);
    }


    public void QuitDemo() {
        Application.Quit();
    }
}


