﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AudioManager 
{
    public static void PlaySound(AudioClip soundClip, float soundVolume) {
        GameObject soundGameObject = new GameObject("Sound");
        AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();
        audioSource.volume = soundVolume;
        audioSource.PlayOneShot(soundClip);
        UnityEngine.Object.Destroy(soundGameObject,soundClip.length);
    }

    public static void PlaySound(AudioClip soundClip, float soundVolume, Vector3 soundPosition) {
        GameObject soundGameObject = new GameObject("Sound");
        soundGameObject.transform.position = soundPosition;
        AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();
        audioSource.spatialBlend = 1;
        audioSource.volume = soundVolume;
        audioSource.PlayOneShot(soundClip);
        UnityEngine.Object.Destroy(soundGameObject, soundClip.length);
    }

    public static GameObject PlayLoopedSound(AudioClip soundClip, float soundVolume) {
        GameObject soundGameObject = new GameObject("LoopedSound");
        AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();
        audioSource.loop = true;
        audioSource.volume = soundVolume;
        audioSource.PlayOneShot(soundClip);
        return soundGameObject;
    }

}
